Rails.application.routes.draw do
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  root 'posts#index'
  resources :posts, only: [ :new, :create, :show, :index]
end
