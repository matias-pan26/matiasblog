class Post < ActiveRecord::Base
  # Callbacks
  before_save :markdown

  # Validations
  validates :title, presence: true, length: { maximum: 50 }
  validates :body, presence: true

  # Associations
  has_many :taggings
  has_many :tags, through: :taggings

  def tag_list
	tags = self.tags.collect do |tag|
	  tag.title
	end
	tags.to_a
  end

  def tag_list=(tags_string)
	tag_names = tags_string.split(',').collect { |t| t.strip.downcase }.uniq
	new_or_found_tags = tag_names.collect { |title| Tag.find_or_create_by(title: title) }
	self.tags = new_or_found_tags
  end

  protected
	def markdown
	  render_options = {
		filter_html:     true,
		hard_wrap:       true,
		link_attributes: { rel: 'nofollow' }
	  }
	  renderer = ApplicationHelper::HTML.new(render_options)

	  extensions = {
		autolink:           true,
		fenced_code_blocks: true,
		lax_spacing:        true,
		no_intra_emphasis:  true,
		strikethrough:      true,
		superscript:        true
	  }
	  self.body = Redcarpet::Markdown.new(renderer, extensions).render(self.body).html_safe
	end
end
