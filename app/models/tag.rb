class Tag < ActiveRecord::Base
  validates :title, presence: true
  # Associations
  has_many :taggings
  has_many :posts, through: :taggings
end
