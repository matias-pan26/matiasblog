class PostsController < ApplicationController
  # before_action :require_login , only: [ :new , :create ]

  def new
	@post = Post.new
  end

  def create
	@post = Post.new(post_params)
	if @post.save
	  flash[:success] = "Post successfully created!"
	  redirect_to @post
	else
	  render :new
	end
  end

  def show
	@post = Post.find(params[:id])
  end

  def index
	@posts = Post.all
	@tags = Tag.all
  end


  private
	def post_params
	  params.require(:post).permit(:title, :body, :draft, :tag_list, :id)
	end

	def require_login
	  redirect_to root_url unless logged_in?(current_user)
	  true
	end
end
