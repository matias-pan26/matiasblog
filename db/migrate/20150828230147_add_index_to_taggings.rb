class AddIndexToTaggings < ActiveRecord::Migration
  def change
	add_index :taggings, :post_id
	add_index :taggings, :tag_id
	add_index :taggings, [ :tag_id, :post_id ], unique: true
  end
end
